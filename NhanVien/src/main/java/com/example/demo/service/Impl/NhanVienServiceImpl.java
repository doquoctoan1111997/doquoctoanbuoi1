package com.example.demo.service.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.NhanVienDto;
import com.example.demo.entity.NhanVien;
import com.example.demo.repository.NhanVienRepository;
import com.example.demo.service.NhanVienService;

@Service
public class NhanVienServiceImpl implements NhanVienService {
	@Autowired
	NhanVienRepository nhanVienRepository;

	@Override
	public List<NhanVienDto> getAllNhanVienDtos() {
		List<NhanVien> liNhanViens = nhanVienRepository.findAll();
		List<NhanVienDto> liNhanVienDtos = new ArrayList<NhanVienDto>();
		for (NhanVien nhanVien : liNhanViens) {
			NhanVienDto nhanVienDto = new NhanVienDto();
			nhanVienDto.setId(nhanVien.getId());
			nhanVienDto.setName(nhanVien.getName());
			liNhanVienDtos.add(nhanVienDto);
		}

		return liNhanVienDtos;
	}

	@Override
	public NhanVienDto getNhanVienDto(int id) {
		NhanVien nhanVien = nhanVienRepository.getById(id);
		NhanVienDto nhanVienDto = new NhanVienDto();
		nhanVienDto.setId(nhanVien.getId());
		nhanVienDto.setName(nhanVien.getName());
		return nhanVienDto;
	}

	@Override
	public void addNhanVien(NhanVienDto nhanVienDto) {

		NhanVien nhanVien = new NhanVien();
		nhanVien.setId(nhanVienDto.getId());
		nhanVien.setName(nhanVienDto.getName());
		nhanVienRepository.save(nhanVien);

	}

	@Override
	public void editNhanVien(NhanVienDto nhanVienDto) {
		NhanVien nhanVien = nhanVienRepository.getById(nhanVienDto.getId());
		if (nhanVien != null) {
			nhanVien.setName(nhanVienDto.getName());
			nhanVienRepository.save(nhanVien);
		}

	}

	@Override
	public void deleteNhanVien(int id) {
		NhanVien nhanVien = nhanVienRepository.getById(id);
		nhanVienRepository.delete(nhanVien);

	}

}
