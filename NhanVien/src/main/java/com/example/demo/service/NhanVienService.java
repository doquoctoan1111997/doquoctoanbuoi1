package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.NhanVienDto;

public interface NhanVienService {
	public List<NhanVienDto> getAllNhanVienDtos();

	public NhanVienDto getNhanVienDto(int id);

	public void addNhanVien(NhanVienDto nhanVienDto);

	public void editNhanVien(NhanVienDto nhanVienDto);

	public void deleteNhanVien(int id);

}
