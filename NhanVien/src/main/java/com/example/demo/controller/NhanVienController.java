package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.NhanVienDto;
import com.example.demo.service.NhanVienService;

@RestController
public class NhanVienController {
	@Autowired
	NhanVienService nhanVienService;

	@GetMapping("/nhanviens")
	public List<NhanVienDto> liNhanVienDTOs() {
		return nhanVienService.getAllNhanVienDtos();
	}

	@GetMapping("/nhanvien/{manv}")
	public NhanVienDto getNhanVienDTO(@PathVariable("manv") int manv) {
		return nhanVienService.getNhanVienDto(manv);

	}

	@DeleteMapping("/nhanvien/{manv}")
	public void deleteNhanVien(@PathVariable("manv") int manv) {
		nhanVienService.deleteNhanVien(manv);
	}

	@PostMapping("/nhanvien")
	public void addNhanVien(@RequestBody NhanVienDto nhanVienDTO) {
		nhanVienService.addNhanVien(nhanVienDTO);
	}

	@PutMapping("/nhanvien")
	public void updateNhanVien(@RequestBody NhanVienDto nhanVienDTO) {

		nhanVienService.editNhanVien(nhanVienDTO);

	}
}
